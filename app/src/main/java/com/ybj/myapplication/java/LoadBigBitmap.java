package com.ybj.myapplication.java;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapRegionDecoder;
import android.graphics.Rect;
import android.widget.ImageView;

import com.ybj.myapplication.kotlin.base.BaseApplication;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by 杨阳洋 on 2020/8/4.
 */
public class LoadBigBitmap {
    //以下指示伪代码
    private void loadBigBitmapToView(){
        try {
            Context context = new BaseApplication().getApplicationContext();
            InputStream inputStream = context.getAssets().open("bigMap.jpg");
            //获取图片宽高但不加载到内存中
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(inputStream,null,options);
            int imgWidth = options.outWidth;
            int imgHeight = options.outHeight;
            //获取BitmapRegionDecoder
            BitmapRegionDecoder bitmapRegionDecoder = BitmapRegionDecoder.newInstance(inputStream, false);
            BitmapFactory.Options mOptions = new BitmapFactory.Options();
            mOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;
            //显示图片
            Rect rect = new Rect(imgWidth / 2 - 400, imgHeight / 2 - 400, imgWidth / 2 + 400, imgHeight / 2 + 400);
            Bitmap bitmap = bitmapRegionDecoder.decodeRegion(rect, mOptions);
            new ImageView(context).setImageBitmap(bitmap);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
