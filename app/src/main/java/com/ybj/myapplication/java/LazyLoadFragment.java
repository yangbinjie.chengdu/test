package com.ybj.myapplication.java;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

/**
 * Created by 杨阳洋 on 2020/8/3.
 * Fragment懒加载
 */
public abstract class LazyLoadFragment extends Fragment {
    
    private boolean isViewInitiated;
    private boolean isDataLoaded;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        isViewInitiated = true;
        prepareRequestData();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        prepareRequestData();
    }

    public boolean prepareRequestData(boolean forceUpdate){
        if(getUserVisibleHint() && isViewInitiated && (!isDataLoaded || forceUpdate)) {
            requestData();
            isDataLoaded = true;//数据已经加载
            return true;
        }
        return false;
    }

    public abstract void requestData();

    public boolean prepareRequestData() {
        return prepareRequestData(false);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        isViewInitiated = false;
    }
}
