package com.ybj.myapplication.java;

import androidx.fragment.app.Fragment;

/**
 * Created by 杨阳洋 on 2020/8/3.
 */
public abstract class LazyFragment extends Fragment {

    private boolean isLoaded = false;

    @Override
    public void onResume() {
        super.onResume();
        judgeLazyInit();
    }
    //可见false
    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        judgeLazyInit();
    }

    private void judgeLazyInit(){
        if(!isLoaded && !isHidden()) {
            lazyInit();
            isLoaded = true;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        isLoaded = false;
    }

    public abstract void lazyInit();

}
