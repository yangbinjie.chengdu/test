package com.ybj.myapplication.designmode;

/**
 * Created by 杨阳洋 on 2020/8/3.
 * 抽象建造者：抽象Builder类，用于规范产品的组建
 */
public abstract class Builder {

    public abstract void buildCpu(String cpu);
    public abstract void buildMainboard(String mainboard);
    public abstract void buildRam(String ram);
    public abstract Computer2 create();
}
