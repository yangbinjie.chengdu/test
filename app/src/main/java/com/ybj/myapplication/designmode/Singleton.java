package com.ybj.myapplication.designmode;

/**
 * Created by 杨阳洋 on 2020/8/3.
 */
public class Singleton {

    private static volatile Singleton instance = null;

    public Singleton() {
    }

    public static Singleton getInstance(){
        //第一次判空省去不必要的同步
        if(instance == null) {
            synchronized (Singleton.class){
                //只有在Singleton等于null时才会生效
                if(instance == null) {
                    instance = new Singleton();
                }
            }
        }
        return instance;
    }

    public static Singleton getInstance2(){
        return SingletonHolder.sInstance;
    }

    private static class SingletonHolder{
        private static final Singleton sInstance = new Singleton();
    }

}
