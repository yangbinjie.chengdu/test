package com.ybj.myapplication.designmode;

/**
 * Created by 杨阳洋 on 2020/8/3.
 * 抽象组件：接口/抽象类，被装饰的最原始的对象。
 */
public abstract class Swordsman {
    public abstract void attackMagic();
}
