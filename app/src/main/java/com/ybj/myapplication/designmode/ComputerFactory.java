package com.ybj.myapplication.designmode;

/**
 * Created by 杨阳洋 on 2020/8/3.
 * 工厂类：负责创建所有实例的内部逻辑，由外界直接调用
 */
public class ComputerFactory {

    public static Computer createComputer(String type){
        Computer computer = null;
        switch (type){
            case "Lenovo":
                computer = new LenovaComputer();
                break;
        }
        return computer;
    }

}
