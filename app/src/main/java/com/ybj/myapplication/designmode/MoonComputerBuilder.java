package com.ybj.myapplication.designmode;

/**
 * Created by 杨阳洋 on 2020/8/3.
 * 具体建造类：实现抽象Builder类的所有方法，并返回建造好的对象。
 */
public class MoonComputerBuilder extends Builder{
    @Override
    public void buildCpu(String cpu) {

    }

    @Override
    public void buildMainboard(String mainboard) {

    }

    @Override
    public void buildRam(String ram) {

    }

    @Override
    public Computer2 create() {
        return null;
    }
}
