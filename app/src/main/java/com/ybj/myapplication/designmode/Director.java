package com.ybj.myapplication.designmode;

/**
 * Created by 杨阳洋 on 2020/8/3.
 * 导演类：负责安排已有模块的安装顺序，最后通知建造者开始建造。
 */
public class Director {

    Builder builder = null;

    public Director(Builder builder) {
        this.builder = builder;
    }

    public Computer2 createComputer(String cpu,String mainboard,String ram){
        this.builder.buildCpu(cpu);
        this.builder.buildMainboard(mainboard);
        this.builder.buildRam(ram);
        return builder.create();
    }

}
