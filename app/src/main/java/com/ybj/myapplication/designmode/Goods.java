package com.ybj.myapplication.designmode;

/**
 * Created by 杨阳洋 on 2020/8/3.
 * 具体享元角色：实现抽象襄垣角色的任务
 */
public class Goods implements IGoods {

    private String name;
    private String price;

    public Goods(String name) {
        this.name = name;
    }

    @Override
    public void showGoodPrice(String name) {

    }
}
