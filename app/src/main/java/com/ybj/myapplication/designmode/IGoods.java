package com.ybj.myapplication.designmode;

/**
 * Created by 杨阳洋 on 2020/8/3.
 * 抽象享元角色：定义对象内部和外部状态的接口。
 */
public interface IGoods {

    public void showGoodPrice(String name);

}
