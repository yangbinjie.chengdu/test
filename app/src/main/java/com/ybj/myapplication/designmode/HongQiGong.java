package com.ybj.myapplication.designmode;

/**
 * Created by 杨阳洋 on 2020/8/3.
 */
public class HongQiGong extends Master {
    public HongQiGong(Swordsman swordsman) {
        super(swordsman);
    }

    public void teachAttackMagic(){}

    @Override
    public void attackMagic() {
        super.attackMagic();
        teachAttackMagic();
    }
}
