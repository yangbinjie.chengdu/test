package com.ybj.myapplication.designmode;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by 杨阳洋 on 2020/8/3.
 */
public class GoodsFactory {

    private static Map<String,Goods> pool = new HashMap<>();

    public static Goods getGoods(String name){
        if(pool.containsKey(name)) {
            return pool.get(name);
        }else{
            Goods goods = new Goods(name);
            pool.put(name,goods);
            return goods;
        }
    }

}
