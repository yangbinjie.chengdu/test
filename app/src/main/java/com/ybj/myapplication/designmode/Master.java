package com.ybj.myapplication.designmode;

/**
 * Created by 杨阳洋 on 2020/8/3.
 * 抽象装饰者：扩展抽象组件的功能
 */
public abstract class Master extends Swordsman{
    private Swordsman swordsman;

    public Master(Swordsman swordsman) {
        this.swordsman = swordsman;
    }

    @Override
    public void attackMagic() {
        swordsman.attackMagic();
    }
}
