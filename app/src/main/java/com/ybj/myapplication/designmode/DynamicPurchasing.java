package com.ybj.myapplication.designmode;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * Created by 杨阳洋 on 2020/8/3.
 */
public class DynamicPurchasing implements InvocationHandler {

    private Object object;

    public DynamicPurchasing(Object object) {
        this.object = object;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        return method.invoke(object,args);
    }
}
