package com.ybj.myapplication.designmode;

import java.lang.reflect.Proxy;

/**
 * Created by 杨阳洋 on 2020/8/3.
 */
public class Client {

    public static void main(String[] args) {
        IShop shop =new JsonChao();
        IShop purchasing = new Purchasing(shop);
        purchasing.buy();

        DynamicPurchasing dynamicPurchasing = new DynamicPurchasing(shop);
        ClassLoader classLoader = shop.getClass().getClassLoader();
        IShop newProxyInstance = (IShop) Proxy.newProxyInstance(classLoader, new Class[]{IShop.class}, dynamicPurchasing);
        newProxyInstance.buy();

    }

}
