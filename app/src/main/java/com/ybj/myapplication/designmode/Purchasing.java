package com.ybj.myapplication.designmode;

/**
 * Created by 杨阳洋 on 2020/8/3.
 * 代理类：持有真实主题类的引用
 */
public class Purchasing implements IShop {

    private IShop shop;

    public Purchasing(IShop shop) {
        this.shop = shop;
    }

    @Override
    public void buy() {
        shop.buy();
    }
}
