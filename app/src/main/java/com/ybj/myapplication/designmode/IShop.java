package com.ybj.myapplication.designmode;

/**
 * Created by 杨阳洋 on 2020/8/3.
 * 抽象主题类：声明真实主题和代理的共同接口方法
 */
public interface IShop {
    void buy();
}
