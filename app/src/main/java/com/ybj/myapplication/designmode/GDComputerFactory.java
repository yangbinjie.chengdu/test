package com.ybj.myapplication.designmode;

/**
 * Created by 杨阳洋 on 2020/8/3.
 */
public class GDComputerFactory extends ComputerFactory2 {
    @Override
    public <T extends Computer> T createComputer(Class<T> clz) {
        Computer computer = null;
        String classname = clz.getName();
        try {
            computer = (Computer) Class.forName(classname).newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return (T) computer;
    }
}
