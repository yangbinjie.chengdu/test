package com.ybj.myapplication.designmode;

/**
 * Created by 杨阳洋 on 2020/8/3.
 * 抽象工厂类：返回一个泛型的产品对象
 */
public abstract class ComputerFactory2 {
    public abstract <T extends Computer> T createComputer(Class<T> clz);
}
